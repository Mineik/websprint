import React from "react"
import { Layout } from "../components/layout"
import { ReservationForm } from "../components/reservationForm"
//tady si testuju formulář, dont @me
export class Page2 extends React.Component {
	public render() {
		//return <Layout>Page 2</Layout>
		return (
			<>
				<Layout>
					<h1>Rezervace místa v zasedačce</h1>
				</Layout>
				<ReservationForm
					velkáZas="Velká zasedačka"
					maláZas="Malá zasedačka"
				/>
			</>
		)
	}
}
