import React from "react"
import { Layout } from "../components/layout"
import Head from "next/head"
import { ReservationForm } from "../components/reservationForm"

export class Index extends React.Component {
	public render() {
		return (
			<>
				<Head>
					<title>Rezervace zasedaček</title>
					<link rel="stylesheet" href="/index.css" />
				</Head>

				<Layout>
					<ReservationForm
						velkáZas="Velká zasedačka"
						maláZas="Malá zasedačka"
					/>
				</Layout>
			</>
		)
	}
}
