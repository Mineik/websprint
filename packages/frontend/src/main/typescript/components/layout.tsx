import React from "react"

export class Layout extends React.Component {
	public render() {
		return (
			<>
				<div className="comm-head">Rezervace místností</div>
				{this.props.children}
			</>
		)
	}
}
