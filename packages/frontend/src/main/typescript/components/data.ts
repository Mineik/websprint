//sem import františkovo kód

export function extractData() {
	const person: string = (document.getElementById(
		"personName",
	) as HTMLInputElement).value
	const firm: string = (document.getElementById(
		"firmName",
	) as HTMLInputElement).value
	const radios = document.getElementsByName("zas") as NodeListOf<
		HTMLInputElement
	>
	const date = (document.getElementById("datum") as HTMLInputElement).value
	const time = (document.getElementById("cas") as HTMLInputElement).value
	let selected = ""

	if (radios[0].checked) selected = radios[0].value
	else if (radios[1].checked) selected = radios[1].value

	const dateA = date.split("-")
	const timeA = time.split(":")
	const dateTimeStr = new Date(
		parseInt(dateA[0]),
		parseInt(dateA[1]),
		parseInt(dateA[2]),
		parseInt(timeA[0]),
		parseInt(timeA[1]),
	).toDateString()

	console.log("Person: ", person)
	console.log("Firm: ", firm)
	console.log("Room: ", selected)
	console.log(dateTimeStr)
}
