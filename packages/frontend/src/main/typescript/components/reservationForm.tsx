import React from "react"
import { extractData } from "./data"

export class ReservationForm extends React.Component<{
	velkáZas: string
	maláZas: string
}> {
	public render() {
		return (
			<>
				<div id="mainform" className="order-form">
					Vaše jméno: <input id="personName" name="Jméno" /> <br />
					Firma: <input id="firmName" name="Firma" /> <br />
					Datum: <input id="datum" type="date" />
					Čas: <input id="cas" type="time" /> <br />
					<label>
						{this.props.velkáZas}
						<input
							id="radioVelka"
							type="radio"
							value="Velka"
							name="zas"
						/>
					</label>
					<label>
						{this.props.maláZas}
						<input
							id="radioMala"
							type="radio"
							value="Mala"
							name="zas"
						/>
					</label>
					<button
						onClick={() => {
							extractData()
						}}
					>
						Poslat
					</button>
				</div>
			</>
		)
	}
}
